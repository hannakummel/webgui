# Dynamische Benutzeroberflächen

### Booklet vor dem Semester

    - Bei jedem Modul steht drinnen, was sie vorab ansehen sollen;
    - Aus Webprogrammierung kennt ihr XXX
    - Aus WINF kennt ihr XXX
    


### Einführung, Feb. 13.02. || 14.02. 4 Lektionen                                (Ulrich)

    - Aufsetzen Unterrichtsumgebung / Entwicklungsumgebung
        - Gitlab (Oder Github und Travis CI)
        - Lokal: IDE &amp; Libraries installieren (Sublime; npm; ..)


### HTML & CSS: Markup for the GUIs we love | 14.02. Nachmittag , 15.02.Vormittag;      (Hanna | 1-2 Sitzungen)

    - Markup Languages  (Theorie)
        - Auffrischen der Kenntnisse;
        - CSS3, SCSS3, SASS;

    - HTML5, HAML5, Mustache

    - Side-Lesson: Debugging via Browser: Chrome Dev Tools (Praxis) (Alexandra; Corsin)
      - Inherited & User Agent Styles erkennen
      - Styles hinzufügen und bearbeiten
      - Fehler erkennen und debuggen
      - Box Modell und errechnete Styles


### Principles of Continuous Integration  22. Februar Nachm. (Ulrich)
    - Prinzipien
    - Continuous Integration services (Travis CI, JetBrains, Atlassian ; Circle CI ; Gitlab)


### Responsive Webdesign    15. 03               Nachmittag              (Hanna) 
      - Mobile First Development
      - Dynamische Anwendungen im Web
      - Gerüst der Seite erstellen mit Bootstrap (Webseite mit 9-10 Seitentemplates): Navigation; Gridsystem mit Containern; Cross Browser Testing


### Javascript: Beyond Basics     29.03. Nachmittag                     (Ulrich)
    - Interpreted Programming Languages
      - Auffrischen der Kenntnisse
      - Libraries (TBA)
      - Asynchronous Web: Ajax
        - An dieser Stelle auf Debugging eingehen!!


### APIs: Accessing the WWW        26.04.  Vormittag                     (Hanna)
    - Application Programming Interfaces
      - Was ist es?
      - Welche APIs gibt es?7 (Google Maps, Youtube, Flickr, Twitter; )
      - Wie sieht eine API Dokumentation aus?8 Qualität v. APIs
      - API: Welche passt zu unserem Projekt, wie kann sie eingebunden werden?

### The Dynamic Web: Frameworks die Bewegung bringen 14.05.             (Ulrich)
    -  Nützliche Frameworks
        -  Three.js, chart.JS, WebGL, d3.js;

### Üben Üben Üben 24.05. Vormittag (Hanna)

### Der Server: Node.js        05.06. über Mittag            (Ulrich | optional)
    - ...
    
           
### Links
    1 https://about.gitlab.com/product/continuous-integration/
    2 https://teamtreehouse.com/tracks/front-end-web-development
    3 http://sass-lang.com/documentation/file.SCSS\_FOR\_SASS\_USERS.html
    4 https://sass-lang.com/
    5 http://haml.info/
    6 https://mustache.github.io/
    7 https://www.programmableweb.com/apis
    8 https://developers.google.com/maps/documentation/javascript/tutorial
